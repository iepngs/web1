$(".banner-area").hover(function(){
	$("#nav-menu").slideUp(200);
	$("#banner-area img").attr('src', "requires/images/banner/"+$(this).data('area')+".jpg");
	$("#nav-menu").stop(true, false).slideDown(400);
}, function(){
	$("#nav-menu").slideUp(400, function(){
		$("#nav-menu").stop(true, false).hide();
	});
})
$("#cart").hover(function(){
	$("#empty-cart").slideUp(200).stop(true, false).slideDown(400);
},function(){
	$("#empty-cart").slideUp(400, function(){
		$("#empty-cart").stop(true, false).hide();
	});
})
$('.carousel').carousel();